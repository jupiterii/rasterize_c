cmake_minimum_required(VERSION 3.17)
project(PerspectiveProjection)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -lmingw32")

set(SOURCE_FILES main.cpp)
add_executable(PerspectiveProjection ${SOURCE_FILES})

target_link_libraries(PerspectiveProjection SDL2main SDL2)